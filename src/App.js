import { useState, useEffect } from "react";
import { Route, Routes, useLocation } from "react-router-dom";
import Login from "./Login";
import MainRoutes from "./MainRoutes";
import Header from "./UiUtilities/Header";
import Sidebar from "./UiUtilities/Sidebar";

function App() {

  const [sidebarOpen, setSidebarOpen] = useState(false);
  const location = useLocation();

  useEffect(() => {
    document.querySelector("html").style.scrollBehavior = "auto";
    window.scroll({ top: 0 });
    document.querySelector("html").style.scrollBehavior = "";
  }, [location.pathname]);

  return (
    <>
      {location.pathname === "/" ? (
        <Routes>
          <Route path="/" element={<Login />} />
        </Routes>
      ) : (
        <div className="flex h-screen bg-slate-300 overflow-hidden">
          <Sidebar
            sidebarOpen={sidebarOpen}
            setSidebarOpen={setSidebarOpen}
          />
          <div className="relative flex flex-col flex-1 overflow-y-auto overflow-x-hidden">
            <Header sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
            <main className="no-scrollbar overflow-auto">
              <div className="px-3 pt-6  pb-10 w-full max-w-10xl mx-auto">
                <MainRoutes />
              </div>
            </main>
          </div>
        </div>
      )}
    </>
  );
}

export default App;
