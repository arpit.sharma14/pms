import React from "react";
import { useNavigate } from "react-router-dom";

function Login() {

  const navigate = useNavigate();

  return (
    <div className="bg-gradient-to-br from-pink-300 via-purple-300 to-indigo-400 h-screen flex">
      <div className="container px-5 py-14 md:py-24 mx-auto my-auto flex flex-wrap items-center ">
        <div className="lg:w-3/5 md:w-1/2 md:pr-16 lg:pr-0 pr-0 space-y-3">
          <h6 className="font-bold text-2xl lg:text-5xl lg:leading-tight text-gray-700">
            Performance <br/> Management <br/> Software
          </h6>
        </div>

        <div className="relative lg:w-[30%] md:w-1/2 w-full">
          <div className="absolute inset-0 bg-gradient-to-r from-blue-300 to-blue-600 shadow-lg transform -skew-y-12 sm:skew-y-0 sm:-rotate-[8deg] rounded-3xl drop-shadow-md"></div>

          <form onSubmit={()=>{navigate('/dashboard')}} className="bg-gray-50 rounded-2xl drop-shadow-md p-8 flex flex-col md:ml-auto  mt-10 md:mt-0 space-y-4">
            <div>
              <img
                src="./HRMusterLogo.png"
                className="h-14"
                alt="HR Muster Logo"
              />
            </div>
            <h2 className="text-gray-800 text-2xl font-medium ">
              Login To Your Account
            </h2>
            <div className="space-y-2">
              <label>Username</label>
              <input type="email" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 outline-blue-500 focus:border-blue-500 block w-full p-2.5 " placeholder="Username" />
            </div>
            <div className="space-y-2">
              <label>Password</label>
              <input type="password" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 outline-blue-500 focus:border-blue-500 block w-full p-2.5 " placeholder="Password" />
            </div>

            <div className="text-right">
              <button className="text-blue-600 hover:underline dark:text-blue-500">
                Forgot Password?
              </button>
            </div>
            <button type="submit" className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center ">
              Submit
            </button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default Login;
