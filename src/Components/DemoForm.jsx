import React, {
    useState,
    useRef,
    useEffect,
    useMemo,
    useCallback,
  } from "react";
  import {
    Accordion,
    AccordionButton,
    AccordionIcon,
    AccordionPanel,
    AccordionItem,
    Input,
    Button,
    Switch,
    useToast,
  } from "@chakra-ui/react";
  import { AgGridReact } from "ag-grid-react";
  import { LuEdit } from "react-icons/lu";
  import axios from "axios";

const DemoForm = () => {
    const toast = useToast();
    const id = "toast";
  
    const [DepartmentID, setDepartmentID] = useState(0);
    const [DepartmentName, setDepartmentName] = useState();
  
    const gridRef = useRef();
    const [rowData, setRowData] = useState([]); // Table Data Variable
    const gridStyle = useMemo(() => ({ height: "100%", width: "100%" }), []); //Ag Grid Styling
    const [columnDefs, setColumnDefs] = useState([
      {
        headerName: "Department Name",
        field: "departmentName",
      },
  
      {
        headerName: "IsActive",
        field: "isActive",
        cellRenderer: (params) => (
          <Switch
            colorScheme="green"
            isChecked={params.data.isActive}
            onChange={({ target }) => {
              HandleDepartmentSwitch(target.checked, params.data);
            }}
            size="md"
          />
        ),
      },
      {
        headerName: "Action",
        field: "guId",
        cellRenderer: (params) => (
          <div className="space-x-4">
            <Button
              onClick={() => {
                setDepartmentID(params.data.departmentId);
                setDepartmentName(params.data.departmentName);
              }}
              variant="solid"
              size="xs"
              colorScheme="blue"
              leftIcon={<LuEdit size="14px" />}
            >
              Edit
            </Button>
          </div>
        ),
      },
    ]);
  
    // Table Pagination
    const paginationNumberFormatter = useCallback((params) => {
      return "[" + params.value.toLocaleString() + "]";
    }, []);
  
    //Table columns properties applies to all columns
    const defaultColDef = useMemo(() => ({
      flex: 1,
      sortable: true,
      filter: "agTextColumnFilter",
      floatingFilter: true,
      cellClass: "no-border",
    }));
  
    useEffect(() => {
      getDepartmentList();
    }, []);
  
    const getDepartmentList = async () => {
      try {
        const response = await axios.get(
          `${process.env.REACT_APP_PMS_URL}/api/Master/DepartmentMasterList?CompanyId=1&BranchId=1`
        );
        console.log("Department List", response.data);
        setRowData(response.data);
      } catch (error) {
        console.error(error);
      }
    };
  
    const HandleDepartmentSwitch = async (checked, departData) => {
      console.log("Department Data", departData);
      setRowData((prev) => {
        const newState = prev.map((obj) => {
          if (obj.departmentId === departData.departmentId) {
            return { ...obj, isActive: checked };
          }
          return obj;
        });
        return newState;
      });
  
      let body = {
        departmentId: departData.departmentId,
        srNo: departData.srNo,
        departmentName: departData.departmentName,
        isActive: checked,
        companyId: 1,
        branchId: 1,
      };
  
      console.log(body);
  
      await axios
        .post(
          `${process.env.REACT_APP_PMS_URL}/api/Master/SaveDepartmentMaster`,
          body
        )
        .then((response) => {
          console.log(response);
          const res = response.data;
          if (!toast.isActive(id)) {
            toast({
              id,
              title: res,
              position: "top",
              status: "success",
              duration: 2000,
              isClosable: true,
            });
          }
          getDepartmentList();
        })
        .catch((error) => {
          console.error(error);
          if (!toast.isActive(id)) {
            toast({
              id,
              title: "ERROR",
              description: `Department not Added`,
              position: "top",
              status: "error",
              duration: 2000,
              isClosable: true,
            });
          }
        });
    };
  
    const SaveDepartmentMaster = async (e) => {
      e.preventDefault();
      let body = {
        departmentId: DepartmentID,
        departmentName: DepartmentName,
        isActive: true,
        companyId: 1,
        branchId: 1,
      };
      console.log(body);
      await axios
        .post(
          `${process.env.REACT_APP_PMS_URL}/api/Master/SaveDepartmentMaster`,
          body
        )
        .then((response) => {
          console.log(response);
          const res = response.data;
          if (!toast.isActive(id)) {
            toast({
              id,
              title: res,
              position: "top",
              status: "success",
              duration: 2000,
              isClosable: true,
            });
          }
  
          getDepartmentList();
          setDepartmentName("");
          setDepartmentID(0);
        })
        .catch((error) => {
          console.error(error);
  
          if (!toast.isActive(id)) {
            toast({
              id,
              title: "ERROR",
              description: `Department not Added`,
              position: "top",
              status: "error",
              duration: 2000,
              isClosable: true,
            });
          }
        });
    };
  
    return (
      <div>
        <div className="border-b border-slate-400 pb-2 mb-4">
          <h1 className="text-xl font-bold text-gray-800">Department Master</h1>
        </div>
  
        <div className="mb-6">
          <Accordion
            defaultIndex={[0]}
            shadow="lg"
            border="transparent"
            rounded="xl"
            allowMultiple
            bg="white"
          >
            <AccordionItem>
              <h2>
                <AccordionButton>
                  <h6 className="text-xl text-left  flex-1 font-bold  text-gray-800">
                    Add Department
                  </h6>
                  <AccordionIcon />
                </AccordionButton>
              </h2>
              <AccordionPanel pb={4}>
                <form onSubmit={SaveDepartmentMaster}>
                  <div className="space-y-2 w-1/3">
                    <label>Add Department Name</label>
                    <Input
                      isRequired
                      value={DepartmentName || ""}
                      onChange={({ target }) => {
                        setDepartmentName(target.value);
                      }}
                      borderColor="gray"
                      placeholder="Add Department Name"
                    />
                  </div>
  
                  <div className="space-x-3 my-2 text-right">
                    <Button
                      onClick={() => {
                        setDepartmentName("");
                        setDepartmentID(0);
                      }}
                      type="reset"
                      variant="outline"
                      colorScheme="purple"
                    >
                      Reset
                    </Button>
                    <Button type="submit" colorScheme="purple">
                      Save
                    </Button>
                  </div>
                </form>
              </AccordionPanel>
            </AccordionItem>
          </Accordion>
        </div>
  
        <div className="ag-theme-alpine">
          <AgGridReact
            style={gridStyle}
            domLayout={"autoHeight"}
            ref={gridRef} // Ref for accessing Grid's API
            rowData={rowData} // Row Data for Rows
            columnDefs={columnDefs} // Column Defs for Columns
            defaultColDef={defaultColDef} // Default Column Properties
            animateRows={true} // Optional - set to 'true' to have rows animate when sorted
            pagination={true}
            paginationPageSize={10}
            paginationNumberFormatter={paginationNumberFormatter}
          />
        </div>
      </div>
    );
  }

export default DemoForm