import StatCard from "../Utils/StatCard";
import { FaUsers } from "react-icons/fa";

const Dashboard = () => {
  return (
    <div>
      <div className="grid grid-cols-4 gap-4">
        <StatCard
          StatIcon={<FaUsers className="text-3xl text-orange-600" />}
          IconBg={"bg-orange-200"}
          StatName={"Total Employees"}
          Description={"-"}
          StatNumber={"100"}
        />
        <StatCard
          StatIcon={<FaUsers className="text-3xl text-orange-600" />}
          IconBg={"bg-orange-200"}
          StatName={"Total Employees"}
          Description={"-"}
          StatNumber={"100"}
        />
        <StatCard
          StatIcon={<FaUsers className="text-3xl text-orange-600" />}
          IconBg={"bg-orange-200"}
          StatName={"Total Employees"}
          Description={"-"}
          StatNumber={"100"}
        />
        <StatCard
          StatIcon={<FaUsers className="text-3xl text-orange-600" />}
          IconBg={"bg-orange-200"}
          StatName={"Total Employees"}
          Description={"-"}
          StatNumber={"100"}
        />
      </div>
    </div>
  );
};

export default Dashboard;
