import { Routes, Route } from "react-router-dom";
import Dashboard from "./Components/Dashboard";
import DemoForm from "./Components/DemoForm";

const MainRoutes = () => {
  return (
    <div>
      <Routes>
        <Route path="/dashboard" element={<Dashboard />} />
        <Route path="/demoform" element={<DemoForm />} />
      </Routes>
    </div>
  )
}

export default MainRoutes